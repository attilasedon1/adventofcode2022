
import { Day } from "../day";

class Day15 extends Day {

    constructor(){
        super(15);
    }

  private static getSensors(input: string): [Sensor[], number, number]{
    let minColumn = Infinity;
    let maxColumn = -Infinity;
    
    const sensors = input.split('\n').slice(0, -1).map(lines => {
      const words = lines.split(' ');
      const xs = words.filter(w => w[0] === 'x').map(e => e.replace(',', '').split('=')[1]).map(n => parseInt(n));
      const ys = words.filter(w => w[0] === 'y').map(e => e.split('=')[1]).map(n => parseInt(n));
      const sensor = new Sensor(xs[0], ys[0], new Beacon(xs[1], ys[1]));
      minColumn = Math.min(minColumn, xs[0]-sensor.getMaxManhattanDistance());
      maxColumn = Math.max(maxColumn, xs[0]+sensor.getMaxManhattanDistance());
      return sensor;
    });
    return [sensors, minColumn, maxColumn];
  }

  solveForPartOne(input: string): string {
    const [sensors, minColumn, maxColumn] = Day15.getSensors(input);
    let res = 0;
    for(let i = minColumn; i <= maxColumn; i++){
      res += (sensors.some(s => s.isInRange(i, 2000000)) ? 1 : 0);
      res -= (sensors.some(s => s.nearestBeacon.x === i && s.nearestBeacon.y === 2000000) ? 1 : 0);
    }
    return res.toString();
  }

  solveForPartTwo(input: string): string {
    const [sensors] = Day15.getSensors(input);
    // create lines from all sensors range + and add +1/-1 depending position, so that it runs out of their range. (there is only one possible solution)
    // Easier to just create the constants
    // y = x + sy-sx+r+1 //a1
    // y = x + sy-sx-r-1 //a2
    // y = -x + sx+sy+r+1 //b1
    // y = -x + sx+sy-r-1 ///b2
    const intersectionConstants = (s: Sensor): number[][] => {
      return [[s.y-s.x+s.getMaxManhattanDistance()+1, s.y-s.x-s.getMaxManhattanDistance()-1], [s.x+s.y+s.getMaxManhattanDistance()+1, s.x+s.y-s.getMaxManhattanDistance()-1]]  
    }
    const allAConstants: Set<number> = new Set();
    const allBConstants: Set<number> = new Set();
    sensors.forEach(e => {
      const [[a1, a2], [b1, b2]] = intersectionConstants(e);
      allAConstants.add(a1);
      allAConstants.add(a2);
      allBConstants.add(b1);
      allBConstants.add(b2);
    });
    const allAConstantsArray = Array.from(allAConstants);
    const allBConstantsArray = Array.from(allBConstants); 
    // Find all intersections (lines will intersect at (b-a)/2 , (a+b)/2)
    const intersections: number[][] = [];
    for(let i = 0; i< allAConstantsArray.length; i++){
      for(let j = 0; j < allBConstantsArray.length; j++){
        const a = allAConstantsArray[i];
	const b = allBConstantsArray[j];
	if((b-a)%2===0){
	  intersections.push([(b-a)/2, (a+b)/2]);
	}
      }
    }
    // Check all intersections to see if it all sensors are out of range
    const foundCoord = intersections.find(e => e[0] >= 0 && e[0]<= 4000000 && e[1] >= 0 && e[1] <= 4000000 && sensors.every(s => !s.isInRange(e[0], e[1])));
    return foundCoord ? ((foundCoord[0]*4000000)+foundCoord[1]).toString() : 'Not found';
  }
}
// 130712040000002703981

export default new Day15;


abstract class Element{
  x: number;
  y: number;
  constructor(x: number,y: number){
    this.x = x;
    this.y = y;
  }

  public static getManhattanDistance(x1: number, x2: number, y1: number, y2: number): number{
    return Math.abs(x1-x2)+Math.abs(y1-y2);
  }
}

class Beacon extends Element {
  constructor(x: number, y: number){
    super(x, y);
  }
}

class Sensor extends Element {
  nearestBeacon: Beacon;
  constructor(x: number, y: number, beacon: Beacon){
    super(x, y);
    this.nearestBeacon = beacon;
  }

  getMaxManhattanDistance(): number{
    return Element.getManhattanDistance(this.nearestBeacon.x, this.x, this.nearestBeacon.y, this.y);
  }
  isInRange(x: number, y: number): boolean{
    return this.getMaxManhattanDistance() >= Element.getManhattanDistance(x, this.x, y, this.y); 
  }
}
