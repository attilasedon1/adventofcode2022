"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const day_1 = require("../day");
class Day6 extends day_1.Day {
    constructor() {
        super(6);
    }
    static solver(input, num) {
        const letters = input.split('').slice(0, -1);
        ;
        for (let i = num; i <= letters.length; i++) {
            const slice = letters.slice(i - num, i);
            const sliceSet = new Set(slice);
            if (sliceSet.size === num) {
                return i.toString();
            }
        }
        return "No solution";
    }
    solveForPartOne(input) {
        return Day6.solver(input, 4);
    }
    solveForPartTwo(input) {
        return Day6.solver(input, 14);
    }
}
exports.default = new Day6;
