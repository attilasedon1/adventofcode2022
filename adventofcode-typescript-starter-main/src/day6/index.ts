
import { Day } from "../day";

class Day6 extends Day {

    constructor(){
        super(6);
    }

  private static solver(input: string, num: number): string {
    const letters = input.split('').slice(0, -1);;
    for(let i = num; i <= letters.length; i++){
      const slice = letters.slice(i-num, i);
      const sliceSet = new Set(slice);
      if(sliceSet.size === num){
	return i.toString();
      }
    }
    return "No solution";
    
  }

  solveForPartOne(input: string): string {
    return Day6.solver(input, 4);
  }


  solveForPartTwo(input: string): string {
    return Day6.solver(input, 14);
  }
}

export default new Day6;
