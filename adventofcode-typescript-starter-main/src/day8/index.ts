
import { Day } from "../day";

class Day8 extends Day {

    constructor(){
        super(8);
    }

  private static getTreeGrid(input:string): number[][]{
    return input.split('\n').slice(0, -1).map(row => row.split('').map(e => parseInt(e)));
  }

  solveForPartOne(input: string): string {
    const treeHeightGrid = Day8.getTreeGrid(input);
    const visibilityGrid = Array.from(treeHeightGrid, row => Array.from(row, () => 0));
    const maxRowTop = treeHeightGrid[0].slice();
    const maxRowBottom = treeHeightGrid[treeHeightGrid.length-1].slice();
    treeHeightGrid.forEach((row, rowIndex) => {
      if(rowIndex === 0 || rowIndex === treeHeightGrid.length-1){
	visibilityGrid[rowIndex].fill(1);
      } else {
	// from left
	let leftMax = row[0];
	visibilityGrid[rowIndex][0] = 1;
	for(let i = 1; i < row.length-1; i++){
	  if(row[i] > leftMax){
	    visibilityGrid[rowIndex][i] = 1;
	    leftMax = row[i];
	  }
	}
	// from right
	let rightMax = row[row.length-1];
	visibilityGrid[rowIndex][row.length-1] = 1;
	for(let j = row.length-2; j > 0; j--){
	  if(row[j] > rightMax){
	    visibilityGrid[rowIndex][j] = 1;
	    rightMax = row[j];
	  }
	}
	// from top
	for(let k = 1; k < row.length-1; k++){
	  if(row[k] > maxRowTop[k]){
	    visibilityGrid[rowIndex][k] = 1;
	    maxRowTop[k] = row[k];
	  }
	}
      }
    });
    for(let g = treeHeightGrid.length-2; g > 0; g--){
      for(let h = 1; h < treeHeightGrid[g].length-1; h++){
	if(treeHeightGrid[g][h] > maxRowBottom[h]){
	  visibilityGrid[g][h] = 1;
	  maxRowBottom[h] = treeHeightGrid[g][h];
	}
      }
    }
    return visibilityGrid.reduce((acc, curr) => acc+curr.reduce((a, c) => a+c, 0), 0).toString();
  }


  solveForPartTwo(input: string): string {
    const treeGrid = Day8.getTreeGrid(input);
    let maxScenic = 0;
    treeGrid.forEach((row, rowIndex) => {
      row.forEach((tree, index) => {
	//to right
	let rightCount = -1;
	let r = index+1;
	for(; r < row.length; r++){
	  if(row[r] >= tree){
	    rightCount = r-index;
	    break;
	  }
	}
	rightCount = rightCount < 0 ? (row.length-1)-index: rightCount;

	// to left
	let leftCount = -1;
	let l = index-1;
	for(; l >= 0; l--){
	  if(row[l] >= tree){
	    leftCount = index-l;
	    break;
	  }
	}
	leftCount = leftCount < 0 ? index: leftCount;

	// to top
	let topCount = -1;
	let t = rowIndex+1;
	for(; t < treeGrid.length; t++){
	  if(treeGrid[t][index] >= tree){
	    topCount = t-rowIndex;
	    break;
	  }
	}
	topCount = topCount < 0 ? (treeGrid.length-1)-rowIndex : topCount;

	let bottomCount = -1;
	let b = rowIndex-1;
	for(; b >= 0; b--){
	  if(treeGrid[b][index] >= tree){
	    bottomCount = rowIndex-b;
	    break;
	  }
	}
	bottomCount = bottomCount < 0 ? rowIndex : bottomCount;
	
	const scenic = rightCount*leftCount*topCount*bottomCount;
	if(scenic > maxScenic) {
	console.log(rightCount, leftCount, topCount, bottomCount);
	  maxScenic = scenic;
	}
      })
    })
    return maxScenic.toString();
  }
}

export default new Day8;
