"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const day_1 = require("../day");
class Day3 extends day_1.Day {
    constructor() {
        super(3);
        this.priorityList = this.generatePriorityList();
    }
    generatePriorityList() {
        const priorityList = {};
        let i = 'a'.charCodeAt(0);
        const z = 'z'.charCodeAt(0);
        for (; i <= z; i++) {
            priorityList[String.fromCharCode(i)] = i - 96;
        }
        let j = 'A'.charCodeAt(0);
        const y = 'Z'.charCodeAt(0);
        for (; j <= y; j++) {
            priorityList[String.fromCharCode(j)] = j - 64 + 26;
        }
        return priorityList;
    }
    findDuplicatedLetter(rucksack) {
        const half = Math.ceil(rucksack.length / 2);
        const firstCompartment = rucksack.slice(0, half);
        const secondCompartment = rucksack.slice(half);
        const [letter] = firstCompartment.split('').filter(e => secondCompartment.indexOf(e) !== -1);
        return letter;
    }
    findDuplicatedLetterInGroup(groupRuckSacks) {
        const intersect = groupRuckSacks[0].split('').filter(e => groupRuckSacks[1].indexOf(e) !== -1);
        const [letter] = intersect.filter(e => groupRuckSacks[2].indexOf(e) !== -1);
        return letter;
    }
    chunk(pieces, array) {
        const groups = [];
        let j = 0;
        let temp = [];
        for (let i = 0; i < array.length; i++) {
            temp.push(array[i]);
            j++;
            if (j % pieces === 0) {
                groups.push(temp);
                j = 0;
                temp = [];
            }
        }
        return groups;
    }
    solveForPartOne(input) {
        return input.split('\n').slice(0, -1)
            .map(e => this.findDuplicatedLetter(e))
            .reduce((acc, curr) => acc + (this.priorityList[curr]), 0)
            .toString();
    }
    solveForPartTwo(input) {
        // Create groups of three
        const group = this.chunk(3, input.split('\n').slice(0, -1))
            .map(e => this.findDuplicatedLetterInGroup(e))
            .reduce((acc, curr) => acc + this.priorityList[curr], 0).toString();
        return group;
    }
}
exports.default = new Day3;
