"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = __importDefault(require("./index"));
describe('On Day 0', () => {
    it(`part1 is identity function`, () => {
        expect(index_1.default.solveForPartOne('hello')).toBe('hello');
    });
});
