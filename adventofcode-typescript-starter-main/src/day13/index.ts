
import { Sign } from "crypto";
import { Day } from "../day";

type SignalType = SignalType[] | number[] | number

class Day13 extends Day {
  constructor(){
    super(13);
  }

  private static checkValue(a: SignalType,b: SignalType): number{
    if(!Array.isArray(a) && !Array.isArray(b))  return a - b
    else{
      if(!Array.isArray(a)) a = [a]
      if(!Array.isArray(b)) b = [b]
      for (var i = 0; i < Math.min(a.length,b.length); i++){
	let res=Day13.checkValue(a[i],b[i]);
	if (res !=0) return res
      }
      return a.length - b.length
    }
}

//console.log("PART 1:",input.reduce((sum,l,i)=>sum+(checkValue(l[0],l[1])>0?0:(i+1)),0))
//
//input.push([[[2]],[[6]]])
//part2 = input.flat().sort((a,b)=>checkValue(a,b)).map(x=>x.toString())
//console.log("PART 2:",(part2.indexOf('2')+1)*(part2.indexOf('6')+1))


  // 1 is false, 0 needs more check, -1 true 
  private static comparePairs(left: SignalType[], right: SignalType[]): number {
    for(let i = 0; i < left.length; i++){
      if(right[i] === undefined){
	return 1;
      }
      if(Array.isArray(left[i]) || Array.isArray(right[i])){
	let leftToUse = left[i];
	let rightToUse = right[i];
	if(!Array.isArray(left[i])){
	  leftToUse = [left[i]]
	}
	if(!Array.isArray(right[i])){
	  right[i] = [right[i]];
	}
	let result = this.comparePairs((leftToUse as SignalType[]), (rightToUse as SignalType[]))
	if(result !== 0){
	  return result;
	}
      }
      const diff = (left[i] as number)-(right[i] as number);
      if(diff !== 0){
	return diff; 
      }
    }
    return -1;
  }

  private static getPairs(input: string): SignalType[][][]{
    return input
      .split('\r')
      .join('')
      .split('\n\n')
      .map(l=>l.split('\n').map(x=>{
	return  x !== '' ? JSON.parse(x) : undefined;
      }).filter(e => e));
  }
  

  solveForPartOne(input: string): string {
    const pairs = Day13.getPairs(input);
    return pairs.reduce((sum,l,i)=>sum+(Day13.checkValue(l[0],l[1])>0?0:(i+1)),0).toString()
    return pairs.map((e, index) => {
      return (Day13.comparePairs(e[0], e[1])<1 ? index+1 : 0)
    }).reduce((acc, curr) => acc+curr, 0).toString();
  }

  solveForPartTwo(input: string): string {
    const signals = Day13.getPairs(input).flat();
    const firstSeparator = [[2]];
    const secondSeparator = [[6]];
    signals.push(firstSeparator);
    signals.push(secondSeparator);
    signals.sort((a,b)=>Day13.checkValue(a,b)).map(x=>x.toString())
    //signals.sort((a, b) => Day13.comparePairs(a as SignalType[], b as SignalType[])) as SignalType[][];
    //console.log(sortedSignals.forEach(e => console.log(JSON.stringify(e))));
    const first = signals.indexOf(firstSeparator)+1
    const second = signals.indexOf(secondSeparator)+1
    console.log(first);
    console.log(second);
    return (first*second).toString();
  }
}

export default new Day13;
