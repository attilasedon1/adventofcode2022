
import { Day } from "../day";

type Stacks = string[][];
type Command = {qty: number, from: number, to: number};

class Day5 extends Day {

	constructor() {
		super(5);
	}



  private static createCommands(commandStr: string): Command[] {
    return commandStr.split('\n').slice(0, -1).map((e) => {
      const sp = e.split(' ');
      return {qty: parseInt(sp[1]), from: parseInt(sp[3])-1, to: parseInt(sp[5])-1}
    });
  }
  
  private static createStacks(stackStr: string): Stacks {
    const stacks: string[][] = [];
    const lines = stackStr.split('\n').slice(0, -1);
    for(let row = 0; row < lines.length; row++){
      for(let i = 1; i < lines[row].length; i=i+4){
	const char = lines[row][i];
	if(char !== ' '){
	  const stackIndex = (i-1)/4;
	  if(stacks[stackIndex] === undefined){
	    stacks[stackIndex] = [];
	  }
	  stacks[stackIndex].push(char);
	}
      }
    }
    return stacks;
  }

  private static moveStacksInReverse = (stacks: Stacks, commands: Command[]): void => {
    commands.forEach(command => {
      stacks[command.to].unshift(...(stacks[command.from].slice(0, command.qty).reverse()));
      stacks[command.from].splice(0,command.qty);
    });
  }

  private static moveStacks = (stacks: Stacks, commands: Command[]): void => {
    commands.forEach(command => {
      stacks[command.to].unshift(...(stacks[command.from].slice(0, command.qty)));
      stacks[command.from].splice(0,command.qty);
    });

  }

  solveForPartOne(input: string): string {
    const [stackStr, commandStr] = input.split('\n\n');
    const stacks = Day5.createStacks(stackStr);
    const commands = Day5.createCommands(commandStr);
    Day5.moveStacksInReverse(stacks, commands);
    return stacks.map(stack => stack[0]).join('');
  }


  solveForPartTwo(input: string): string {
    const [stackStr, commandStr] = input.split('\n\n');
    const stacks = Day5.createStacks(stackStr);
    const commands = Day5.createCommands(commandStr);
    Day5.moveStacks(stacks, commands);
    return stacks.map(stack => stack[0]).join('');
  }
}

export default new Day5;
