
import { finished } from "stream";
import { Day } from "../day";

class Day16 extends Day {

    constructor(){
      super(16);
    }

  private static bfsFromValve(rootNode: Valve): Map<Valve, number>{
    const queue: Valve[] = [];
    const nodeDistanceMap: Map<Valve, number> = new Map();
    const visited: Map<Valve, boolean> = new Map([[rootNode, true]]);
    nodeDistanceMap.set(rootNode, 0);
    queue.push(rootNode);
    while(queue.length > 0){
      let node = (queue.shift() as Valve)
      node.children.forEach(c => {
	if(!visited.get(c)){
	  visited.set(c, true);
	  nodeDistanceMap.set(c, (nodeDistanceMap.get(node) as number)+1);
	  queue.push(c);
	}
      })
    }
    return nodeDistanceMap;
  }

  private static getValvesFromInput(input: string): Valve[] {
    const lines = input.split('\n').slice(0, -1).map(line => line.split(' ').map(w => w.replace(',', '').replace(';', '')));
    const valves = lines.map(line => new Valve(parseInt(line[4].split('=')[1]), line[1]));
    const sliceIndex = lines[0].indexOf('valves');
    lines.forEach((e, index) => {
      const childrenNames = e.slice(sliceIndex+1);
      valves[index].addChildrens(valves.filter(v => childrenNames.includes(v.name)));
    });
    return valves;
  }

  private static getPaths(input: string, minutes: number): Path[]{
    const valves = Day16.getValvesFromInput(input);
    const getActiveValves =  (valves: Valve[]) => valves.filter(e => !e.open && e.flowRate > 0); 

    const nodeDistanceMap: Map<Valve, Map<Valve, number>> = new Map();
    valves.forEach(v => {
      nodeDistanceMap.set(v, Day16.bfsFromValve(v));
    })
    let paths = [new Path((valves.find(v => v.name === 'AA') as Valve), getActiveValves(valves), minutes, false, [], 0)];
    for(let i = 0; i < paths.length; i++){
      let path = paths[i] as Path;
      let distances = nodeDistanceMap.get(path.current) as Map<Valve, number>;
      let moved = false;
      path.active.forEach(v => {
	const dist = distances.get(v) as number
	const newTimeleft = path.timeLeft-(dist+1);
	if(newTimeleft <= 0){
	  return;
	}
	moved = true;
	paths.push(new Path(v, path.active.filter(e => e !== v), newTimeleft, false, [...path.steps, v], path.releasedPressure+(newTimeleft*v.flowRate)))
      });
      if(!moved){
	path.finished = true;
      }
    }
    return paths;
  }
  
  solveForPartOne(input: string): string {
    return Day16.getPaths(input, 30).reduce((acc, curr) => Math.max(acc, curr.releasedPressure), 0).toString();
  }

  solveForPartTwo(input: string): string {
    const paths = Day16.getPaths(input, 26);
    paths.sort((a, b)=> b.releasedPressure-a.releasedPressure);
    let maxReleasedPressure = 0;
    for(let i = 0; i < paths.length; i++){
      for(let j = 0; j < paths.length; j++){
	if(paths[i].steps.every(s => !paths[j].steps.includes(s))){
	  const releasedPressure = paths[i].releasedPressure + paths[j].releasedPressure;
	  if(releasedPressure > maxReleasedPressure){
	    maxReleasedPressure = releasedPressure;
	    console.log(maxReleasedPressure);
	  }
	}
      }
    }
    return maxReleasedPressure.toString();
  }
}

export default new Day16;

class Valve {
  flowRate: number;
  name: string;
  children: Valve[] = [];
  open: boolean = false;

  constructor(flowRate: number, name: string){
    this.flowRate = flowRate;
    this.name = name;
  }

  addChildrens(valves: Valve[]){
    this.children.push(...valves);
  }

  addChildren(valve: Valve){
    this.children.push(valve);
  }
}

class Path {
  current: Valve;
  active: Valve[];
  timeLeft: number;
  finished: boolean;
  steps: Valve[];
  releasedPressure: number;

  constructor(current: Valve, active: Valve[], timeLeft: number, finished: boolean, steps: Valve[], releasedPressure: number){
    this.current = current;
    this.active = active;
    this.timeLeft = timeLeft;
    this.finished = finished;
    this.steps = steps;
    this.releasedPressure = releasedPressure
  }
}
