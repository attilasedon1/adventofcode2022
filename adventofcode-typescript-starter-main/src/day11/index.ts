
import { Day } from "../day";

class Day11 extends Day {

  static ROUND1: number = 20;

  constructor(){
    super(11);
  }

  private static solver(numberOfRounds: number, relief: boolean, input: string): string {
    // Using chinese remainder theorem, creating a hufge modulo from the monkey test numbers (they are relative primes),
    // you can do remainders so that it won't become a really-really big number.
    // If you modulo with that you will get a remainder:
    // if it is divisible by all, you get 0, if not divisible by some of the numbers you get 0+the remainder, but it is still a much smaller number.
    // I tried using just number but it still failed, I guess you really need the bigint for this.
    let supermod: bigint = 1n;
    const monkeys = input.split('\n\n').map(inputElem => {
      const monkey = Monkey.fromText(inputElem)
      supermod *= monkey.getTestNumber();
      return monkey;
    });
    for(let i = 1; i <= numberOfRounds; i++){
      monkeys.forEach(monkey => monkey.throwItem(relief, supermod, monkeys));
    }
    const inspections = monkeys.map(e => e.getNumberOfInspections());
    inspections.sort((a, b) => b-a);
    console.log(inspections);
    return (inspections[0]*inspections[1]).toString();
  }

  solveForPartOne(input: string): string {
    return Day11.solver(20, true, input)
  }

  solveForPartTwo(input: string): string {
    return Day11.solver(10000, false, input);
  }
}

export default new Day11;


class Monkey {
  private numberOfInspections: number = 0;
  private items: bigint[];
  private operation: (a: bigint) => bigint;
  private testNumber: bigint;
  private monkeyFalse: number;
  private monkeyTrue: number;

  constructor(items: bigint[], operation: (a: bigint) => bigint, testNumber: bigint, monkeyFalse: number, monkeyTrue: number) {
    this.items = items;
    this.operation = operation
    this.testNumber = testNumber;
    this.monkeyFalse = monkeyFalse;
    this.monkeyTrue = monkeyTrue;
  }

  getNumberOfInspections(): number{
    return this.numberOfInspections;
  }

  getTestNumber(): bigint {
    return this.testNumber;
  }

  addItem(item: bigint){
    this.items.push(item);
  }

  throwItem(relief: boolean, supermod: bigint, monkeys: Monkey[]): void {
    while(this.items.length > 0){
      this.numberOfInspections++;
      let itemToThrow = (this.items.shift() as bigint);
      itemToThrow = this.operation(itemToThrow);
      itemToThrow = relief ? itemToThrow/3n : itemToThrow;
      itemToThrow %= supermod;
      if(itemToThrow % this.testNumber === 0n){
	monkeys[this.monkeyTrue].addItem(itemToThrow);
      } else {
	monkeys[this.monkeyFalse].addItem(itemToThrow);
      }
    }
  }


  
  
  private static buildOperation = (operationSign: string, operationNumber: string): (a: bigint) => bigint => {
    const numToUse = (num: bigint) =>  (operationNumber === 'old' ? num : BigInt(operationNumber));
    switch(operationSign){
      case '+': return (num: bigint): bigint => num+numToUse(num);
      case '-': return (num: bigint): bigint => num-numToUse(num);
      case '*': return (num: bigint): bigint => num*numToUse(num);
      case '/': return (num: bigint): bigint => num/numToUse(num);
      default: throw Error('Invalid Operation')
    }
  }

  private static getLastNumberFromInput(input: string): number{
    const arr = input.split(' ');
    return parseInt(arr[arr.length-1]);
  }
  
  static fromText(input: string): Monkey {
    const monkeyStr = input.split('\n');
    const startingItems = monkeyStr[1].split(':')[1].split(',').map(e => BigInt(e.trim()));
    const [operationSign, operationNumber] = monkeyStr[2].split('=')[1].trim().split(' ').slice(1);
    return new Monkey(
      startingItems,
      Monkey.buildOperation(operationSign, operationNumber),
      BigInt(Monkey.getLastNumberFromInput(monkeyStr[3])),
      this.getLastNumberFromInput(monkeyStr[5]),
      this.getLastNumberFromInput(monkeyStr[4]));
  }
}
