
import { Day } from "../day";

type Coord = {x: number, y: number};

class Day9 extends Day {

	constructor() {
		super(9);
	}

  private static move(previous: Coord, next: Coord) {
    const verticalDiff = previous.y - next.y;
    const horizontalDiff = previous.x - next.x;
    if (Math.abs(previous.x - next.x) > 1 || Math.abs(previous.y - next.y) > 1) {
        // non-diagonal moves
        // up
      if (previous.x === next.x && previous.y > next.y) {
        next.y += 1;
        // down
      } else if (previous.x === next.x && previous.y < next.y) {
        next.y -= 1;
        // right
      } else if (previous.y === next.y && previous.x > next.x) {
        next.x += 1;
        // left
      } else if (previous.y === next.y && previous.x < next.x) {
        next.x -= 1;

        // diagonal moves
        // up right
      } else if (previous.x > next.x && previous.y > next.y) {
        next.x += 1;
        next.y += 1;
        // down right
      } else if (previous.x > next.x && previous.y < next.y) {
        next.x += 1;
        next.y -= 1;
        // up left
      } else if (previous.x < next.x && previous.y > next.y) {
        next.x -= 1;
        next.y += 1;
        // down left
      } else if (previous.x < next.x && previous.y < next.y) {
        next.x -= 1;
        next.y -= 1;
      }
    }
  }

  private static solver(input: string, numberOfParts: number, index: number): number {
    const commands = input.split('\n').slice(0, -1);
    const headAndTailIndex: Coord[] = Array.from(Array(10), () => ({x: 0, y: 0}));
    const setOfCoords = new Set();
    commands.forEach(command => {
      const [direction, stepsStr] = command.split(' ');
      const steps = parseInt(stepsStr);
      for (let i = 1; i <= steps; i++) {
	switch (direction) {
	  case 'U': headAndTailIndex[0].y++; break;
	  case 'D': headAndTailIndex[0].y--; break;
	  case 'R': headAndTailIndex[0].x++; break;
	  case 'L': headAndTailIndex[0].x--; break;
	}
	for(let i = 1; i < headAndTailIndex.length; i++){
	  Day9.move(headAndTailIndex[i-1], headAndTailIndex[i]);
	}
        setOfCoords.add(`${headAndTailIndex[index].y},${headAndTailIndex[index].x}`);
      }
    })
    return setOfCoords.size;
  }


  solveForPartOne(input: string): string {
    return Day9.solver(input, 2, 1).toString();
  }


  solveForPartTwo(input: string): string {
    return Day9.solver(input, 10, 9).toString();
  }
}

export default new Day9;
