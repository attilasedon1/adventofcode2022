"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const day_1 = require("../day");
class Day2 extends day_1.Day {
    constructor() {
        super(2);
        this.opponentValues = ['A', 'B', 'C'];
        this.yoursValues = ['X', 'Y', 'Z'];
    }
    getArrayFromInput(input) {
        return input.split("\n").slice(0, -1);
    }
    calculateScore(opponentIndex, yoursIndex) {
        let winScore = 0;
        const difference = opponentIndex - yoursIndex;
        if (difference === 2) {
            winScore = 6;
        }
        else if (difference === -2) {
            winScore = 0;
        }
        else if (difference === -1) {
            winScore = 6;
        }
        else if (difference === 1) {
            winScore = 0;
        }
        else {
            winScore = 3;
        }
        return winScore + (yoursIndex + 1);
    }
    calculateMatchups(matchup) {
        const [opponent, yours] = matchup.split(" ");
        const opponentIndex = this.opponentValues.indexOf(opponent);
        const yoursIndex = this.yoursValues.indexOf(yours);
        return this.calculateScore(opponentIndex, yoursIndex);
    }
    calculateRound(matchup) {
        const [opponent, command] = matchup.split(" ");
        const opponentIndex = this.opponentValues.indexOf(opponent);
        let yoursIndex;
        if (command === 'X') {
            yoursIndex = opponentIndex === 0 ? 2 : opponentIndex - 1;
        }
        else if (command === 'Z') {
            yoursIndex = opponentIndex === 2 ? 0 : opponentIndex + 1;
        }
        else {
            yoursIndex = opponentIndex;
        }
        return this.calculateScore(opponentIndex, yoursIndex);
    }
    solveForPartOne(input) {
        return this.getArrayFromInput(input).map(e => this.calculateMatchups(e)).reduce((acc, curr) => acc + curr, 0).toString();
    }
    solveForPartTwo(input) {
        return this.getArrayFromInput(input).map(e => this.calculateRound(e)).reduce((acc, curr) => acc + curr, 0).toString();
    }
}
exports.default = new Day2;
