
import { Day } from "../day";

class Day2 extends Day {

  constructor(){
    super(2);
  }

  private opponentValues = ['A', 'B', 'C'];
  private yoursValues = ['X', 'Y', 'Z'];

  private getArrayFromInput(input: string): string[]{
    return input.split("\n").slice(0,-1);
  }

  private calculateScore(opponentIndex: number, yoursIndex: number): number {
    let winScore = 0;
    const difference = opponentIndex - yoursIndex;
    if(difference === 2) {
      winScore = 6;
    } else if (difference === -2) {
      winScore = 0;
    } else if (difference === -1){
      winScore = 6;
    } else if (difference === 1){
      winScore = 0;
    } else {
      winScore = 3;
    }
    return winScore + (yoursIndex+1)
  }

  private calculateMatchups(matchup: string): number {
    const [opponent, yours] = matchup.split(" ");
    const opponentIndex = this.opponentValues.indexOf(opponent);
    const yoursIndex = this.yoursValues.indexOf(yours);
    return this.calculateScore(opponentIndex, yoursIndex);
  }

  private calculateRound(matchup: string): number{
    const [opponent, command] = matchup.split(" ");
    const opponentIndex = this.opponentValues.indexOf(opponent);
    let yoursIndex;
    if(command === 'X'){
      yoursIndex = opponentIndex === 0 ? 2 : opponentIndex - 1;
    } else if(command === 'Z'){
      yoursIndex = opponentIndex === 2 ? 0 : opponentIndex + 1;
    } else {
      yoursIndex = opponentIndex;
    }
    return this.calculateScore(opponentIndex, yoursIndex);
    
  }
  
  solveForPartOne(input: string): string {
    return this.getArrayFromInput(input).map(e => this.calculateMatchups(e)).reduce((acc, curr) => acc+curr, 0).toString();
  }

  solveForPartTwo(input: string): string { 
    return this.getArrayFromInput(input).map(e => this.calculateRound(e)).reduce((acc, curr) => acc+curr, 0).toString();
  }
}

export default new Day2;
