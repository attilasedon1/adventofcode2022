
import { Day } from "../day";

class Day7 extends Day {

    constructor(){
        super(7);
    }

  // Make it more general with visited
  private buildFilesSystem(node: DirectoryNode, remainder: string[]){
    let current = 0;
    if(remainder[0] === "$ ls"){
      let i = 1;
      while(remainder[i] && remainder[i][0] !== '$'){
	const line = remainder[i];
	if(line[0] === 'd'){
	  node.directories.push(new DirectoryNode(line.split(' ')[1], node));
	} else {
	  const [size, name] = line.split(' ');
	  node.files.push(new Files(name, parseInt(size)));
	}
	i++;
      }
      current = i;
    }
    if(!remainder[current]){
      return;
    }
    const command = remainder[current].split(' '); 
    if(command[1] === 'cd'){
      if(command[2] === '..'){
	if(node.parent !== null){
	  this.buildFilesSystem(node.parent, remainder.slice(current+1));
	} else {
	  throw new Error(`Invalid directory ${remainder[current].toString()}`);
	}
      } else {
	const foundNode = node.directories.find(e => e.name === command[2])
	if(foundNode){
	  this.buildFilesSystem(foundNode, remainder.slice(current + 1));
	} else {
	  throw new Error(`Invalid directory ${remainder[current].toString()} here`);
	}
      }
    }
  }

  private breadthFirstForSizes(array: DirectoryNode[], node: DirectoryNode) {
    const size = node.getSize();
    if(size <= 100000){
      array.push(node);
    }
    node.directories.forEach(childNode => this.breadthFirstForSizes(array, childNode));
  }

  private breadthFirstSearchToDelete(deleteSize: number, arr: DirectoryNode[], node: DirectoryNode): void {
    const size = node.getSize();
    const diff = size - deleteSize;
    if(diff >= 0){
      arr.push(node);
    }
    node.directories.forEach(childNode => this.breadthFirstSearchToDelete(deleteSize, arr, childNode));
  }

  solveForPartOne(input: string): string {
    const outputs = input.split('\n').slice(0, -1);
    const root = new DirectoryNode('/', null);
    this.buildFilesSystem(root, outputs.slice(1));
    const array: DirectoryNode[] = [];
    this.breadthFirstForSizes(array, root);
    console.log(array.reduce((acc, curr) => acc+curr.getSize(), 0));
    return array.reduce((acc, curr) => acc+curr.getSize(), 0).toString();
  }


  solveForPartTwo(input: string): string {
    const outputs = input.split('\n').slice(0, -1);
    const root = new DirectoryNode('/', null);
    this.buildFilesSystem(root, outputs.slice(1));
    const rootSize = root.getSize();
    console.log(rootSize);
    const deleteSize = rootSize - 40000000;
    const del: DirectoryNode[] = [];
    this.breadthFirstSearchToDelete(deleteSize, del, root);
    console.log(del.map(e => e.getSize()));
    return del.map(e => e.getSize()).sort((a,b) => a-b)[0].toString();
    }
}

export default new Day7;

class Files {
  public name: string;
  public size: number;
  constructor(name: string, size: number){
    this.name = name;
    this.size = size;
  }
}

class DirectoryNode {
  public name: string;
  public parent: DirectoryNode | null;
  public directories: DirectoryNode[];
  public files: Files[];
  public getSize(): number {
    return this.directories.reduce((acc, curr) => acc+curr.getSize(), 0) + this.files.reduce((acc, curr) => acc+curr.size,0);
  }
  constructor(name: string, parent: DirectoryNode | null){
    this.name = name;
    this.parent = parent;
    this.directories = [];
    this.files = [];
  }
 }
