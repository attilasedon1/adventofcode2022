"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const day_1 = require("../day");
class Day4 extends day_1.Day {
    constructor() {
        super(4);
    }
    static createArrayFromRange(range) {
        let [start, finish] = range.split('-').map(e => parseInt(e));
        const rangeArray = [];
        for (; start <= finish; start++) {
            rangeArray.push(start);
        }
        return rangeArray;
    }
    static areEqual(array1, array2) {
        return array1.length === array2.length && array1.every((element, index) => element === array2[index]);
    }
    static findFullyContaining(range1, range2) {
        const rangeArr1 = Day4.createArrayFromRange(range1);
        const rangeArr2 = Day4.createArrayFromRange(range2);
        const intersection = rangeArr1.filter(e => rangeArr2.indexOf(e) !== -1);
        return Day4.areEqual(rangeArr1, intersection) || Day4.areEqual(rangeArr2, intersection);
    }
    static findOverlapping(range1, range2) {
        const rangeArr1 = Day4.createArrayFromRange(range1);
        const rangeArr2 = Day4.createArrayFromRange(range2);
        const intersection = rangeArr1.filter(e => rangeArr2.indexOf(e) !== -1);
        return intersection.length !== 0;
    }
    solve(input, solver) {
        return input.split('\n').slice(0, -1).map(e => e.split(',')).map(ranges => solver(ranges[0], ranges[1])).reduce((acc, curr) => curr ? acc + 1 : acc, 0).toString();
    }
    solveForPartOne(input) {
        return this.solve(input, Day4.findFullyContaining);
    }
    solveForPartTwo(input) {
        return this.solve(input, Day4.findOverlapping);
    }
}
exports.default = new Day4;
