
import { Day } from "../day";

class Day4 extends Day {


  constructor(){
    super(4);
  }

  public static createArrayFromRange(range: string): number[] {
    let [start, finish] = range.split('-').map(e => parseInt(e));
    const rangeArray = [];
    for (;start<= finish; start++){
      rangeArray.push(start);
    }
    return rangeArray;
  }

  private static areEqual(array1: any[], array2: any[]): boolean {
    return array1.length === array2.length && array1.every((element, index) => element === array2[index]);
  }

  private static findFullyContaining(range1: string, range2: string): boolean{
    const rangeArr1 = Day4.createArrayFromRange(range1);
    const rangeArr2 = Day4.createArrayFromRange(range2);
    const intersection = rangeArr1.filter(e => rangeArr2.indexOf(e) !== -1);
    return Day4.areEqual(rangeArr1, intersection) || Day4.areEqual(rangeArr2, intersection);
  }

  private static findOverlapping(range1: string, range2: string): boolean {
    const rangeArr1 = Day4.createArrayFromRange(range1);
    const rangeArr2 = Day4.createArrayFromRange(range2);
    const intersection = rangeArr1.filter(e => rangeArr2.indexOf(e) !== -1);
    return intersection.length !== 0;
  }

  private solve(input: string, solver: (range1: string, range2: string) => boolean): string{
    return input.split('\n').slice(0,-1).map(e => e.split(',')).map(ranges => solver(ranges[0], ranges[1])).reduce((acc, curr) => curr ? acc+1 : acc, 0).toString();
  }

  solveForPartOne(input: string): string {
    return this.solve(input, Day4.findFullyContaining);
  }

  solveForPartTwo(input: string): string {
    return this.solve(input, Day4.findOverlapping);
  }
}

export default new Day4;
