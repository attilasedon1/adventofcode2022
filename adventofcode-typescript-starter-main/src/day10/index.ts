import { Day } from "../day";

  class Day10 extends Day {
    constructor(){
        super(10);
    }

    private static checkCycleAndAddSignal(cycle: number, value: number, signalStrength: number[]) {
      if(cycle % 40 === 20){
	signalStrength.push(cycle*value);
      }
    }

    solveForPartOne(input: string): string {
      const commands = input.split('\n').slice(0, -1);
      let registerValue = 1;
      let cycle = 0;
      const signalStrength: number[] = [];
      commands.forEach(command => {
	if(command === 'noop'){
	  cycle++;
	  Day10.checkCycleAndAddSignal(cycle, registerValue, signalStrength);
	} else {
	  const [op, value] = command.split(' ');
	  cycle++;
	  Day10.checkCycleAndAddSignal(cycle, registerValue, signalStrength);
	  cycle++;
	  Day10.checkCycleAndAddSignal(cycle, registerValue, signalStrength);
	  registerValue += parseInt(value);
	}
      });
      return signalStrength.reduce((acc, curr) => acc+curr, 0).toString();
    }

    solveForPartTwo(input: string): string {
      const commands = input.split('\n').slice(0, -1);
      let registerValue = 1;
      let cycle = 0;
      const CRT: string[][] = Array.from(Array(6), () => Array(40).fill(' '));
      let rowIndex = -1;
      const checkAndUpdateRow = () => {
	cycle++;
	const index = cycle % 40;
	if(index === 1) {
	  rowIndex++
	}
	if([registerValue-1, registerValue, registerValue+1].some(e => e === index-1)){
	  CRT[rowIndex][index-1] = '#';
	} else {
	  CRT[rowIndex][index-1] = '.';
	}
      }
      commands.forEach(command => {
	if(command === 'noop'){
	  checkAndUpdateRow()
	} else {
	  const [op, value] = command.split(' ');
	  checkAndUpdateRow()
	  checkAndUpdateRow()
	  registerValue += parseInt(value);
	}
      })
      return CRT.map(row => row.join('')).join('\n');
    }
  }

export default new Day10;
