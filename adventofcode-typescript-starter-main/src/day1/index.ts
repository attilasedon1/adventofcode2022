import { Day } from "../day";

class Day1 extends Day {

  constructor(){
    super(1);
  }

  private calculateAllCalories(input: string): number[] {
    return input.split("\n\n").map(e => e.split('\n')).map(e => e.map(str => parseInt(str)).reduce((acc, curr) => acc+curr, 0));
  }

  solveForPartOne(input: string): string {
    const arrayOfCalories = this.calculateAllCalories(input);
    let max = 0;
    for(let i = 0; i < arrayOfCalories.length; i++){
      if(arrayOfCalories[i] > max){
	max = arrayOfCalories[i];
      }
    }
    return max.toString();
  }

  solveForPartTwo(input: string): string {
    const arrayOfCalories = this.calculateAllCalories(input);
    arrayOfCalories.sort((a, b) => b-a);
    return arrayOfCalories.slice(0,3).reduce((acc, curr) => acc+curr, 0).toString();
  }
}

export default new Day1;
