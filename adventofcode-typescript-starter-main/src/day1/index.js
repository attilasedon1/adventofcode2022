"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const day_1 = require("../day");
class Day1 extends day_1.Day {
    constructor() {
        super(1);
    }
    calculateAllCalories(input) {
        return input.split("\n\n").map(e => e.split('\n')).map(e => e.map(str => parseInt(str)).reduce((acc, curr) => acc + curr, 0));
    }
    solveForPartOne(input) {
        const arrayOfCalories = this.calculateAllCalories(input);
        let max = 0;
        for (let i = 0; i < arrayOfCalories.length; i++) {
            if (arrayOfCalories[i] > max) {
                max = arrayOfCalories[i];
            }
        }
        return max.toString();
    }
    solveForPartTwo(input) {
        const arrayOfCalories = this.calculateAllCalories(input);
        arrayOfCalories.sort((a, b) => b - a);
        return arrayOfCalories.slice(0, 3).reduce((acc, curr) => acc + curr, 0).toString();
    }
}
exports.default = new Day1;
