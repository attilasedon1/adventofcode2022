
import { Day } from "../day";

class Day14 extends Day {


  constructor(){
    super(14);
  }


  private static buildPath(place: string[][], prevRockPath: number[], nextRockPath: number[]): number {
    if(prevRockPath[1] === nextRockPath[1]){
      const y = prevRockPath[1];
      const lower = nextRockPath[0] > prevRockPath[0] ? prevRockPath[0] : nextRockPath[0];
      const higher = nextRockPath[0] > prevRockPath[0] ? nextRockPath[0] : prevRockPath[0];
      for(let i = lower; i <= higher;i++){
	place[y][i] = '#';
      }
      return -1;
    } else {
      const x = prevRockPath[0];
      const lower = nextRockPath[1] > prevRockPath[1] ? prevRockPath[1] : nextRockPath[1];
      const higher = nextRockPath[1] > prevRockPath[1] ? nextRockPath[1] : prevRockPath[1];
      for(let i = lower; i <= higher;i++){
	place[i][x] = '#';
      }
      return higher;
    }
  }

  private static buildAllRockPaths(place: string[][], rockPaths: number[][][]): void {
    let bottomIndex = -1;
    rockPaths.forEach(e => {
      for(let i = 1; i < e.length; i++){
	const higher = this.buildPath(place, e[i-1], e[i]);
	if(higher > bottomIndex){
	  bottomIndex = higher;
	}
      }
    });
    place[bottomIndex+2].fill('#');
    place.splice(bottomIndex+3, place.length-(bottomIndex-3));
  }

  private static buildPlaceAndPath(input: string): string[][]{
    const place = Array.from(Array(200), () => Array(1000).fill('.'));
    const rockPaths = input.split('\n').slice(0, -1).map(e => e.split(' -> ').map(c => c.split(',').map(h => parseInt(h))));
    Day14.buildAllRockPaths(place, rockPaths);
    return place;
  }

  private static getCountOfSand(place: string[][], predicate: (a: number) => boolean): number {
    let countOfSand = 0;
    while(true){
      let columnIndex = 500;
      let rowIndex;
      for(rowIndex = 0; rowIndex < place.length; rowIndex++){
	if(rowIndex === 0 && place[rowIndex][columnIndex] === 'o'){
          console.log(place.map(e => e.join('')).join('\n')+ '\n\n');
	  return countOfSand;
	}
        if(place[rowIndex][columnIndex] !== '.'){
      	  if(place[rowIndex][columnIndex-1] === '.'){
      	    columnIndex--;
      	  } else if(place[rowIndex][columnIndex+1] === '.'){
      	    columnIndex++;
      	  } else {
	    if(predicate(rowIndex)){
              console.log(place.map(e => e.join('')).join('\n')+ '\n\n');
	      return countOfSand;
	    }
      	    place[rowIndex-1][columnIndex] = 'o'
	    //console.log('row: ', rowIndex-1, 'column: ', columnIndex);
	    countOfSand++;
	    //console.log('countOfSand: ', countOfSand);
      	    break;
      	  }
        }
      }
    }
  }

  solveForPartOne(input: string): string {
    const place = Day14.buildPlaceAndPath(input);
    return Day14.getCountOfSand(place, (rowIndex) => rowIndex === place.length-1).toString();
  }

    solveForPartTwo(input: string): string {
      const place = Day14.buildPlaceAndPath(input);
      return Day14.getCountOfSand(place, (rowIndex) => rowIndex === 0).toString();
    }
}

export default new Day14;
