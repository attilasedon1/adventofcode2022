
import { Day } from "../day";

class Day12 extends Day {

    constructor(){
        super(12);
    }
  private static addNeighbours(nodeIndices: [number, number], elevationNodes: Node[][]): void{
    const [row, column] = nodeIndices;
    const node = elevationNodes[row][column];
    const addIfRightElevation = (node: Node, childrenNode: Node): void => {
      const diff = node.getNodeHeightValue()-childrenNode.getNodeHeightValue();
      if(diff >= -1){
	node.addNode(childrenNode);
      }
    }
    if(elevationNodes[row][column-1]){
      addIfRightElevation(node, elevationNodes[row][column-1]);
    }
    if(elevationNodes[row-1] && elevationNodes[row-1][column]){
      addIfRightElevation(node, elevationNodes[row-1][column]);
    }
    if(elevationNodes[row+1] && elevationNodes[row+1][column]){
      addIfRightElevation(node, elevationNodes[row+1][column]);
    }
    if(elevationNodes[row][column+1]){
      addIfRightElevation(node, elevationNodes[row][column+1]);
    }
  }

  solveForPartOne(input: string): string {
    // Setup Nodes
    const elevationNodes = input.split('\n').slice(0, -1).map(row => row.split('').map(e => new Node(e)));
    const nodeMap: Map<Node, number> = new Map()
    elevationNodes.forEach((row, index) => {
      for(let i = 0; i < row.length;i++){
	Day12.addNeighbours([index, i], elevationNodes);
	nodeMap.set(elevationNodes[index][i], -1);
      }
    });
    // Modified BFS
    const rootNode = elevationNodes[0][0];
    let queue: Node[] = [rootNode];
    let visited: Map<Node, boolean> = new Map();
    nodeMap.set(rootNode, 0);
    visited.set(rootNode, true);
    while(queue.length > 0){
      const node = (queue.shift() as Node);
      node.children.forEach(e => {
	if(!visited.get(e)){
	  visited.set(e, true);
	  queue.push(e);
	  nodeMap.set(e, (nodeMap.get(node) as number)+1);
	}
      });
    }
    const Enode = ([...nodeMap.keys()].find(e => e.value === 'E') as Node);
    return (nodeMap.get(Enode) as number).toString();
  }

  solveForPartTwo(input: string): string {
    // Setup Nodes
    const elevationNodes = input.split('\n').slice(0, -1).map(row => row.split('').map(e => new Node(e)));
    const nodeMap: Map<Node, number> = new Map();
    let eIndex = [-1,-1];
    elevationNodes.forEach((row, index) => {
      for(let i = 0; i < row.length;i++){
	if(row[i].value === 'E'){
	  eIndex = [index, i];
	}
	Day12.addNeighbours([index, i], elevationNodes);
	nodeMap.set(row[i], -1);
      }
    });
    // Modified BFS
    let minDistance = Infinity;
    elevationNodes.forEach((row, index) => {
      row.forEach((node, index) => {
	if(node.value === 'a'){
	  const rootNode = node;
	  let queue: Node[] = [rootNode];
	  let visited: Map<Node, boolean> = new Map();
	  nodeMap.set(rootNode, 0);
	  visited.set(rootNode, true);
	  while(queue.length > 0){
	    const node = (queue.shift() as Node);
	    node.children.forEach(e => {
	      if(!visited.get(e)){
		visited.set(e, true);
		queue.push(e);
		nodeMap.set(e, (nodeMap.get(node) as number)+1);
	      }
	    });
	  }
	  const Enode = ([...nodeMap.keys()].find(e => e.value === 'E') as Node);
	  const distance = (nodeMap.get(Enode) as number)
	  if(distance < minDistance){
	    minDistance = distance;
	  }
	}
      })
    });
    return minDistance.toString();
  }
}

export default new Day12;


class Node {
  children: Node[] = [];
  value: string;

  constructor(value: string){
    this.value = value;
  }
  addNode(node: Node): void{
    this.children.push(node);
  }
  getNodeHeightValue() {
    if(this.value === 'E'){
      return 'z'.charCodeAt(0);
    } else {
      return this.value.charCodeAt(0);
    }
  }
}
