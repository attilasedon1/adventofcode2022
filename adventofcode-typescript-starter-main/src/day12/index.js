const temp1 = text.split(/\r?\n/);
function day12(part1 = true) {
    let matrix = [];
    let visited = [];
    let possibleMoves = [[-1, 0], [1, 0], [0, -1], [0, 1]];
    temp1.forEach(x => { matrix.push(x.split(''));
			 visited.push(x.split('')); });
    const boardLength = matrix.length;
    const boardWidth = matrix[0].length;
    let startPos1 = [];
    for (let i = 0; i < matrix.length; i++) {
	//find starting positions
	for (let k = 0; k < matrix[0].length; k++) {
	    if (part1 && matrix[i][k] === 'S') {
		startPos1 = [i, k]; break;
	    }
	    if (!part1 && matrix[i][k] === 'E') {
		startPos1 = [i, k]; break;
	    }
	}
    }
    visited[startPos1[0]][startPos1[1]] = '#';
    let analyzePosition = [[startPos1]];
    let nextStack = [];
    let solution = false;

    function findPath() {
	while (!solution) {
	    let posStack = analyzePosition[analyzePosition.length - 1];
	    nextStack = [];
	    posStack.forEach(currentPos => {
		if (solution) return;
		let level1 = matrix[currentPos[0]][currentPos[1]].charCodeAt(0);
		if (matrix[currentPos[0]][currentPos[1]] === 'S')
		    level1 = 97;
		if (matrix[currentPos[0]][currentPos[1]] === 'E')
		    level1 = 122;
		for (let j = possibleMoves.length - 1; j >= 0; j--) {
		    let m = possibleMoves[j];
		    let tempPos = [currentPos[0] + m[0], currentPos[1] + m[1]];
		    //Check is move is inside board
		    if (tempPos[0] < boardLength && tempPos[0] >= 0 && tempPos[1] >= 0 && tempPos[1] < boardWidth) {
			//Check if elevation is correct
			const level2 = matrix[tempPos[0]][tempPos[1]].charCodeAt(0);
			if (part1 && level2 > level1 + 1) continue;
			if (!part1 && level2 < level1 - 1) continue;
			//Check if target reached
			if (part1 && matrix[tempPos[0]][tempPos[1]] === 'E') {
			    posStack.push(tempPos);
			    console.log("part1 length: ", analyzePosition.length);
			    solution = true;
			    break;
			}
			if (!part1 && matrix[tempPos[0]][tempPos[1]] === 'a') {
			    posStack.push(tempPos);
			    console.log("part2 length: ", analyzePosition.length);
			    solution = true;
			    break;
			}
			//Check if not visited
			if (visited[tempPos[0]][tempPos[1]] !== '#') {
			    visited[tempPos[0]][tempPos[1]] = '#';
			    nextStack.push(tempPos);
			}
		    }
		}
	    }); analyzePosition.push(nextStack);
	    if (nextStack.length <= 0) { console.log('no next move found'); break; }
	}
    } //----
    return { findPath };
}
const part_1 = day12(); part_1.findPath(); const part_2 = day12(false); part_2.findPath();
